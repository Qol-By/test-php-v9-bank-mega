<!-- Array1 berisi "Satu", "Dua", "Tiga","Lima"
Array2 berisi "Tiga","Empat","Lima","Enam"
Object1 berisi "Satu", "Dua", "Tiga","Lima"
Object2 berisi "Tiga","Empat","Lima","Enam" -->
<?php
// Soal 1
function Soal1Array() {
    $Array1 = array("Satu", "Dua", "Tiga" , "Lima");
    $Array2 = array("Tiga","Empat","Lima","Enam");
    $alldata = array_merge($Array1,$Array2);
    sort($alldata);
    return json_encode(array("data" => $alldata, "Statuscode" => 200));
}

function Soal1object() {
    $Array1 = array("Satu", "Dua", "Tiga" , "Lima");
    $Array2 = array("Tiga","Empat","Lima","Enam");
    $alldata = array_merge($Array1,$Array2);
    return json_encode($alldata);
}


?>