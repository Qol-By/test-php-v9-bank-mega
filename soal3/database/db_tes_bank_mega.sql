/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100417
 Source Host           : localhost:3306
 Source Schema         : db_tes_bank_mega

 Target Server Type    : MySQL
 Target Server Version : 100417
 File Encoding         : 65001

 Date: 04/12/2021 13:54:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ms_devisi
-- ----------------------------
DROP TABLE IF EXISTS `ms_devisi`;
CREATE TABLE `ms_devisi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nm_devisi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int(2) NULL DEFAULT 1,
  `CreatedAt` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `UpdateAt` datetime(0) NULL DEFAULT NULL,
  `CreateBy` int(2) NULL DEFAULT NULL,
  `UpdateBy` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ms_devisi
-- ----------------------------
INSERT INTO `ms_devisi` VALUES (1, 'IT', 1, '2021-12-04 11:48:51', NULL, NULL, NULL);
INSERT INTO `ms_devisi` VALUES (2, 'Finance', 1, '2021-12-04 11:48:51', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for ms_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `ms_karyawan`;
CREATE TABLE `ms_karyawan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `umur` int(4) NULL DEFAULT 0,
  `gaji` bigint(20) NULL DEFAULT 0,
  `valuta` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `devisi_id` int(11) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_devisi_karwayan`(`devisi_id`) USING BTREE,
  CONSTRAINT `fk_devisi_karwayan` FOREIGN KEY (`devisi_id`) REFERENCES `ms_devisi` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ms_karyawan
-- ----------------------------
INSERT INTO `ms_karyawan` VALUES (1, '001', 'Anton', 25, 650, 'USD', 2, 1);
INSERT INTO `ms_karyawan` VALUES (2, '002', 'Budi', 35, 545, 'EUR', 2, 1);
INSERT INTO `ms_karyawan` VALUES (3, '003', 'Charles', 30, 7000000, 'IDR', 1, 1);
INSERT INTO `ms_karyawan` VALUES (4, '004', 'Dodi', 27, 900, 'USD', 1, 1);
INSERT INTO `ms_karyawan` VALUES (5, '005', 'Edwin', 41, 10000000, 'IDR', 2, 1);
INSERT INTO `ms_karyawan` VALUES (6, '006', 'Fajar', 20, 750, 'EUR', 2, 1);
INSERT INTO `ms_karyawan` VALUES (7, '123', '123', NULL, NULL, NULL, 1, 1);
INSERT INTO `ms_karyawan` VALUES (8, '12', '12', NULL, NULL, NULL, 1, 1);
INSERT INTO `ms_karyawan` VALUES (12, '1', '2', NULL, NULL, NULL, 1, 1);

-- ----------------------------
-- Table structure for ms_kurs
-- ----------------------------
DROP TABLE IF EXISTS `ms_kurs`;
CREATE TABLE `ms_kurs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `valuta` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kurs` bigint(255) NULL DEFAULT NULL,
  `status` int(4) NULL DEFAULT 1,
  `createdAt` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `UpdateAt` datetime(0) NULL DEFAULT NULL,
  `CreateBy` int(2) NULL DEFAULT NULL,
  `UpdateBy` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ms_kurs
-- ----------------------------
INSERT INTO `ms_kurs` VALUES (1, 'IDR', 1, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ms_kurs` VALUES (2, 'USD', 10000, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ms_kurs` VALUES (3, 'EUR', 9000, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ms_kurs` VALUES (4, 'CNY', 150, 1, NULL, NULL, NULL, NULL);
INSERT INTO `ms_kurs` VALUES (5, 'JPY', 200, 1, NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
