<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Test Bank Mega</title>
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel='stylesheet' href="sweetalert2/dist/sweetalert2.min.css">
</head>

<body>
    <div class="container">
        <h1 style="font-size:20pt">Ajax CRUD (Test Bank Mega)</h1>
        <h3>Data Karyawan</h3>
        <br/>
        <button class="btn btn-success" id="button-add"><i class="glyphicon glyphicon-plus"></i> Add Karyawan</button>
        <br />
        <br />
        <table id="data" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="10%">No</th>
                    <th>NIP</th>
                    <th>Nama</th>
                    <th>Divisi</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <script src="jquery/jquery-3.3.1.min.js"></script>
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js-data/person/get-index.js"></script>


    <!-- Bootstrap modal Add -->
    <div class="modal fade" id="add-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                <h1>Tambah Data Karyawan</h1>
                <form method="post" id="form-add" enctype="multipart/form-data">
                    <div class="row form-group" >
                        <div class="col-md-3">
                            <label>NIP</label>
                        </div>
                        <div class="col-md-6">
                            <input id="nip" class="form-control" type='number'>
                        </div>
                    </div>

                    <div class="row form-group" >
                        <div class="col-md-3">
                            <label>Nama</label>
                        </div>
                        <div class="col-md-6">
                            <input id="nama" class="form-control" type='text'>
                        </div>
                    </div>

                    <div class="row form-group" >
                        <div class="col-md-3">
                            <label>Devisi</label>
                        </div>
                        <div class="col-md-6">
                            <select type="text" id="devisi" class="form-control">
                                <option value="">Pilih Devisi</option>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" id="btn_add" class="btn btn-primary">Simpan</button>
                    </div>

                </form>

            </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap modal -->

    <div class="modal fade" id="edit_modal" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form method="post" id="form-update" enctype="multipart/form-data">
                        <input type="hidden" class="form-control" name="edit_id" id="edit_id">
                        <div class="row form-group" >
                            <div class="col-md-3">
                                <label>NIP</label>
                            </div>
                            <div class="col-md-6">
                                <input id="edit_nip" class="form-control" type='number'>
                            </div>
                        </div>

                        <div class="row form-group" >
                            <div class="col-md-3">
                                <label>Nama</label>
                            </div>
                            <div class="col-md-6">
                                <input id="edit_nama" class="form-control" type='text'>
                            </div>
                        </div>

                        <div class="row form-group" >
                            <div class="col-md-3">
                                <label>Devisi</label>
                            </div>
                            <div class="col-md-6">
                                <select type="text" id="edit_devisi" class="form-control">
                                </select>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" id="btn_update" class="btn btn-primary">Update</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

</html>