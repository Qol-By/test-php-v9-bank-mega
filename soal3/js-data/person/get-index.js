(function ($) {
    let baseURL = "";
    let host = location.host;

    function base_url() {
        let pathparts = location.pathname.split('/');
        let url;
        if ( host == 'localhost' ) {
            url = location.origin + '/' + pathparts[1].trim('/') + '/';
        }else{
            url = location.origin  + '/' ; // prod
        }
        return url;
    }

    baseURL =  base_url();
    let showLoading = function () {
        Swal.fire({
            title: 'Mohon Tunggu!',
            html: 'Proses...',
            allowOutsideClick: false,
            showConfirmButton: false,
            willOpen: () => {
                Swal.showLoading()
            },
        });
    }

    const loadingScreen = function () {
        Swal.fire({
            title: 'Mohon Tunggu!',
            html: 'Proses...',
            timer: 1000,
            timerProgressBar: true,
            allowOutsideClick: false,
            showConfirmButton: false,
            didOpen: () => {
                Swal.showLoading();
            }
        });
    }

    loadData()

    function loadData() {
        $.ajax({
            url: baseURL + "ajax_data.php",
            dataType: "JSON",
            beforeSend: function() {
                loadingScreen();
            },
            success: function (response) {
                setTimeout(function () {
                    createTable(response, 0);
                }, 500);
            },
            error: function (jqXHR, exception) {
            setTimeout(function () {
                $("#data tbody").empty();
                let tr = "";
                if (jqXHR.status === 0) {
                    tr = `<tr>
                            <td colspan='13' class='text-center'><i class='fa fa-exclamation-circle fa-lg text-danger' style='margin-top: 15px;'><span class='errorFound'>Not connect, Verify Network.</span></i></td>
                        </tr>`;
                } else if (jqXHR.status == 404) {
                    tr = `<tr>
                            <td colspan='13' class='text-center'><i class='fa fa-exclamation-circle fa-lg text-danger' style='margin-top: 15px;'><span class='errorFound'>Requested page not found. [404]</span></i></td>
                        </tr>`;
                } else if (jqXHR.status == 500) {
                    tr = `<tr>
                            <td colspan='13' class='text-center'><i class='fa fa-exclamation-circle fa-lg text-danger' style='margin-top: 15px;'><span class='errorFound'>Internal Server Error [500].</span></i></td>
                        </tr>`
                } else if (exception === "parsererror") {
                    tr = `<tr>
                            <td colspan='13' class='text-center'><i class='fa fa-exclamation-circle fa-lg text-danger' style='margin-top: 15px;'><span class='errorFound'>Requested JSON parse failed.</span></i></td>
                        </tr>`;
                } else if (exception === "timeout") {
                    tr = `<tr>
                            <td colspan='13' class='text-center'><i class='fa fa-exclamation-circle fa-lg text-danger' style='margin-top: 15px;'><span class='errorFound'>Time out error.</span></i></td>
                        </tr>`;
                } else if (exception === "abort") {
                    tr = `<tr>
                            <td colspan='13' class='text-center'><i class='fa fa-exclamation-circle fa-lg text-danger' style='margin-top: 15px;'><span class='errorFound'>Ajax request aborted.</span></i></td>
                        </tr>`;
                } else {
                    tr = `<tr>
                            <td colspan='13' class='text-center' > <i class='fa fa-exclamation-circle fa-lg text-danger' style='margin-top: 15px;'><span class='errorFound'>Uncaught Error. ${jqXHR.responseText}</span></i></td>
                            </tr>`;
                }

                $("#data tbody").append(tr);
            }, 500);
            },
        });
    }

    function createTable(result, sno) {
        let tr;
        if (result.length > 0) {
            sno = Number(sno);
            $("#data tbody").empty();
            let index;
            for (index in result) {
                let id = result[index].id;
                let nama = result[index].nama;
                let nm_devisi = result[index].nm_devisi;
                let nip = result[index].nip;

                sno += 1;
                tr = `<tr>
                            <td>${sno}</td>
                            <td>${nip}</td>
                            <td>${nama}</td>
                            <td>${nm_devisi}</td>
                            <td>
                                <button type="button" class="edit_data btn text-warning" data-id="${id}">Edit</button>
                                <button type="button" class="delete_data btn text-danger" data-id="${id}">Hapus</button>
                            </td>
                        </tr>`
                $("#data tbody").append(tr);
            }
        } else {
            $("#data tbody").empty();
            tr = `<tr>
                    <td colspan='10' class='text-center'><i class='fa fa-search-minus fa-lg text-secondary' style='margin-top: 15px;'><span class='errorFound'>Data empty</span></i></td>
                </tr>`;
            $("#data tbody").append(tr);
        }
    }

    function getDevisi(type, iddevisi = "",) {
        $.ajax({
            url: baseURL + "get_devisi.php",
            dataType: "json",
            success: function (result) {
                for (index in result) {
                    let devisi = result[index].id;
                    if (iddevisi == devisi) {
                        $(`${type}`).append(`<option value="${devisi}" selected>  ${result[index].nm_devisi}  </option>`);
                    } else {
                        $(`${type}`).append(`<option value="${devisi}">  ${result[index].nm_devisi}  </option>`);
                    }
                }
            },
        });
    }

    $(document).on('click', '#button-add', function(event) {
        event.preventDefault();
        getDevisi("#devisi");

        $('#form-add')[0].reset();
        $('#add-modal').modal('show'); // show bootstrap modal
    });

    $(document).on('click', '#btn_add', function(e) {
        e.preventDefault();
        const nip = $('#nip ').val();
        const nama = $('#nama').val();
        const devisi = $('#devisi').val();

        if (nip.length < 1) {
            ErrorFormKosong("Nip Tidak Boleh Kosong");
            return false;
        }
        if (nama.length < 1) {
            ErrorFormKosong("Nama Tidak Boleh Kosong");
            return false;
        }
        if (devisi == "") {
            ErrorFormKosong("Harap Pilih");
            return false;
        }
        $('#btn_add').text('Proses...');
        $('#btn_add').attr('disabled', true);
        $.ajax({
            url: baseURL + "proses_save.php",
            type: "POST",
            dataType: "json",
            data: {
                nip: nip,
                nama: nama,
                devisi_id : devisi
            },
            beforeSend: function() {
                showLoading();
            },
            success: function (response, jqXHR) {
                if (response.status == "success") {
                    $("#add-modal").modal('hide');
                    $('#btn_add').text('Simpan');
                    $('#btn_add').attr('disabled', false);

                    Swal.fire({
                        icon: 'success',
                        title: 'Berhasil!',
                        html: 'Data Berhasil Ditambahkan',
                        timer: 1000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                        },
                    }).then((result) => {
                        if (result.dismiss === Swal.DismissReason.timer) {
                            loadData();
                        } else {
                            loadData();
                        }
                    })
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Terjadi Kesalahan Pada Saat Simpan Data'
                    });
                    $('#btn_add').text('Simpan');
                    $('#btn_add').attr('disabled', false);
                }


            },
            error: function (xhr, textStatus) {
                if (xhr.status == 400) {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Semua Field Harus Diisi Semua'
                    });
                    $('#btn_add').text('Simpan');
                    $('#btn_add').attr('disabled', false);
                    return false;
                }
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...' + xhr.status,
                    text: 'Something Wrong'
                });
                $('#btn_add').text('Simpan');
                $('#btn_add').attr('disabled', false);
            },
        });
    });

    $(document).on('click', 'button.edit_data', function(e) {
        e.preventDefault();
        let id = $(this).data('id');
        $.ajax({
            url: baseURL + "get_by_id.php",
            method: "post",
            data:{
                id : id
            },
            dataType: "JSON",
            success: function (response) {
                console.log(response);
                $('#edit_nip').attr('disabled',true);
                $("#edit_modal").modal('show');
                $('#edit_id').val(response.id);
                $('#edit_nama').val(response.nama);
                getDevisi("#edit_devisi", response.devisi_id);
                getDevisi(response.devisi_id)
                $('#edit_nip').val(response.nip);
            },
            error: function (jqXHR, exception) {

            },
        });

    });

    $(document).on('click', '#btn_update', function(e)  {
        e.preventDefault();
        const id = $('#edit_id').val();
        const nama = $('#edit_nama').val();
        const devisi = $('#edit_devisi').val();

        if (nama.length < 1) {
            ErrorFormKosong("Nama Tidak Boleh Kosong");
            return false;
        }

        if (devisi == "") {
            ErrorFormKosong("Devisi Tidak Boleh Kosong");
            return false;
        }

        $('#btn_update').text('proses...');
        $('#btn_update').attr('disabled', true);

        $.ajax({
            url: baseURL + "proses_edit.php",
            type: "POST",
            dataType: "JSON",
            data: {
                id: id,
                nama: nama,
                devisi: devisi,
            },
            beforeSend: function() {
                showLoading();
            },
            success: function (response) {
                if (response.status == "success") {
                    $("#edit_modal").modal('hide');
                    $('#btn_update').text('Simpan');
                    $('#btn_update').attr('disabled', false);

                    Swal.fire({
                        icon: 'success',
                        title: 'Berhasil!',
                        html: 'Data Berhasil Update',
                        timer: 1000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                        },
                    }).then((result) => {
                        if (result.dismiss === Swal.DismissReason.timer) {
                            loadData();
                        } else {
                            loadData();
                        }
                    })
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Terjadi Kesalahan Pada Saat Simpan Data'
                    });
                    $('#btn_update').text('Simpan');
                    $('#btn_update').attr('disabled', false);
                }


            },
            error: function (xhr, textStatus) {
                if (xhr.status == 400) {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: 'Semua Field Harus Diisi Semua'
                    });
                    $('#btn_update').text('Simpan');
                    $('#btn_update').attr('disabled', false);
                    return false;
                }
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...' + xhr.status,
                    text: 'Something Wrong'
                });
                $('#btn_update').text('Simpan');
                $('#btn_update').attr('disabled', false);
            },
        });

    });

    $(document).on('click', 'button.delete_data', function(event) {
        event.preventDefault();

        var id = $(this).data('id');

        Swal.fire({
            title: 'Konfirmasi',
            text: "Apakah Anda Yakin Menghapus Kategori Barang Ini?",
            showCancelButton: true,
            confirmButtonText: 'Hapus',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url:  baseURL + "proses_delete.php",
                    method: "post",
                    data:{
                        id : id
                    },
                    beforeSend: function() {
                        showLoading();
                    },
                    success: function (response) {
                        if (response.status == "success") {
                            Swal.fire({
                                icon: 'success',
                                title: 'Berhasil!',
                                html: 'Data Berhasil Dihapus',
                                timer: 1000,
                                timerProgressBar: true,
                                didOpen: () => {
                                    Swal.showLoading()
                                },
                            }).then((result) => {
                                if (result.dismiss === Swal.DismissReason.timer) {
                                    loadData();
                                } else {
                                    loadData();
                                }
                            })
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Terjadi Kesalahan Pada Saat Delete Data'
                            });
                        }


                    },
                    error: function (xhr, textStatus) {
                        if (xhr.status == 400) {
                            Swal.fire({
                                icon: 'warning',
                                title: 'Oops...',
                                text: 'Semua Field Harus Diisi Semua'
                            });
                            return false;
                        }
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...' + xhr.status,
                            text: 'Something Wrong'
                        });
                    },
                })
            } else if (result.dismiss === swal.DismissReason.cancel) {
                Swal.fire(
                    'Batal',
                    'Anda membatalkan Proses',
                    'error'
                )
            }
        })
    });

    function ErrorFormKosong(string) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: string,
        })
    };


})(jQuery);